package bcas.td.mynummatrix;

public class MyMatrixDemo {
	public static void main(String[] args) {

		MyMatrix myMatrix = new MyMatrix(2, 2);
		int[][] matrixA = myMatrix.createRandom2DArray();
		int[][] matrixB = myMatrix.createRandom2DArray();

		myMatrix.printMatrix(matrixA);
		System.out.println("**************\n");
		myMatrix.printMatrix(matrixB);
		System.out.println("**************\n");
		myMatrix.printMatrix(myMatrix.sumMatrix(matrixA, matrixB));
		System.out.println(" \n");
		myMatrix.printMatrix(myMatrix.sumMatrix1(matrixA, matrixB));
		System.out.println("**************\n");
		myMatrix.printMatrix(myMatrix.sumMatrix2(matrixA, matrixB));
		System.out.println("**************\n");

	}

}
