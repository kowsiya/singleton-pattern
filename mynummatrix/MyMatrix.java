package bcas.td.mynummatrix;

import java.util.Random;

public class MyMatrix {
	private int row, col;
	Random rand = new Random();

	public MyMatrix(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public int[][] createRandom2DArray() {
		int matrix[][] = new int[row][col];
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				int randValue = rand.nextInt(50);
				matrix[r][c] = randValue;
				if (randValue < 100) {
					matrix[r][c] = randValue;
				} else {
					c--;
				}
			}
		}
		return matrix;
	}

	public void printMatrix(int matrix[][]) {
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				System.out.print("[" + matrix[r][c] + "]");
			}
			System.out.println();
		}
	}

	public int[][] sumMatrix(int[][] matrixA, int[][] matrixB) {

		int sumMatrix[][] = new int[row][col];
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				sumMatrix[r][c] = (matrixA[r][c] * matrixB[c][r]);
			}
		}
		return sumMatrix;
	}

	public int[][] sumMatrix1(int[][] matrixA, int[][] matrixB) {

		int sumMatrix1[][] = new int[row][col];
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				sumMatrix1[r][c] = matrixA[r][c] * matrixB[c][r];
			}
		}
		return sumMatrix1;
	}

	public int[][] sumMatrix2(int[][] matrixA, int[][] matrixB) {

		int sumMatrix2[][] = new int[row][col];
		for (int r = 0; r < row; r++) {
			for (int c = 0; c < col; c++) {
				sumMatrix2[r][c] = (matrixA[r][c] * matrixB[c][r]) + (matrixA[r][c] * matrixB[c][r]);
			}
		}
		return sumMatrix2;
	}
}
