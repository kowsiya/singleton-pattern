package Single.Pat;

public class SingleObject {

	private static SingleObject instance = new SingleObject();

	private SingleObject() {

	}

	public static SingleObject getInstance() {
		return instance;
	}

	public void showMge() {
		System.out.println("Hello Friends");
	}

}